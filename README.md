###Development Exercise Overview and Guidelines
Your company has recently released an application that generates the first 10 rows of Pascal's triangle that was received with great enthusiasm. Recently, you've released an updated version that generates the first 30 rows of Pascal's triangle. Since this release, you've been receiving complaints of degraded performance. You've been tasked with figuring out what's causing the performance slowdown as well as providing a fix.

At Engagency, we primarily use git for source control. For this programming exercise, we would like for you to implement the requirements via a fork of our repository. We may use the code review features to ask question and discuss your approach.

Start by creating a fork of this repository in BitBucket and create a branch for your changes (use the naming format {yourInitials}-Engagency.PascalsTriangle.v2). When your work is ready for review then open a pull request against this repository. In general, use source control as you would on a real project: provide concise (but helpful) comments with your commits, try to keep commits fairly focused, etc.

###What we're looking for:

- A brief description of what was causing the issue as well as how it was fixed and why you decided to fix it that way.
- Clean readable code, instead of concentrating on optimizing beyond what a user would notice at the expense of readability.
- Fixing the problem at hand. If you do forsee a possible issue in the future, feel free to mention it and how you would fix the problem, but don't feel the need to implement the fix as part of the exercise.

###Info on Pascal's triangle:
https://en.wikipedia.org/wiki/Pascal%27s_triangle
