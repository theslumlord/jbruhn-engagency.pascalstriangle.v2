﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pascals_Triangle
{
    class Program
    {
        private const int RowsToGenerate = 30;
        private const string OutputFileLocation = @"C:\Temp\pascal.txt"; //modified for my computers drive

        static void Main(string[] args)
        {
            //var rows = Enumerable.Range(0, RowsToGenerate); // items commented out to preserver original functionality.
            //var triangle = rows
            //    .Select(row => Enumerable.Range(0, row + 1)
            //    .Select(column => Enumerable.Range(0, column + 1)));

            long[][] table = new long[RowsToGenerate][];
            GetPascalValue(ref table);

            using (var writer = new StreamWriter(OutputFileLocation))
            {
                foreach (var row in table)
                {
                    writer.WriteLine(string.Join(" ", row));
                }
            }
        }

        static long[][] GetPascalValue(ref long[][] table) // takes the table passed by reference.
        {

            for (int i = 0; i < RowsToGenerate; ++i)
            {
                table[i] = new long[i + 1]; //define number of columns in row
                table[i][0] = 1; //place a 1 on the far left to the triangle
                table[i][i] = 1; //place a 1 on the far right of the triangle.
                if (i > 1) // calculated inner values
                {
                    for (int j = 1; j < i; ++j)
                    {
                        table[i][j] = table[i - 1][j - 1] + table[i - 1][j];
                    }
                }
            }
            return table;
        }
    }
}
